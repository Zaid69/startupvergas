package utilities;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;

/**
 *
 * @author xoxoctic
 */
public class Tools {

    /**
     * Resize image JPG, JPEG o PNG file.
     *
     * @param source original image path
     * @param destino destination file (absolute or relative path)
     * @param strExt extension file (absolute or relative path)
     * @param width for the width of image
     * @param height for the height of image
     * @throws IOException in case of problems writing the file
     * @return boolean if process is successfully
     */
    public static Boolean resizeImagenComplete(String source, String destino, String strExt, int width, int height) throws IOException {
        BufferedImage sourceImage = ImageIO.read(new FileInputStream(source));
        double ratio = (double) sourceImage.getWidth() / sourceImage.getHeight();
        if (width < 1) {
            width = (int) (height * ratio + 0.4);
        } else if (height < 1) {
            height = (int) (width / ratio + 0.4);
        }

        Image scaled = sourceImage.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
        BufferedImage bufferedScaled = new BufferedImage(scaled.getWidth(null), scaled.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = bufferedScaled.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g2d.drawImage(scaled, 0, 0, width, height, null);
        File dest = new File(destino);
        dest.createNewFile();
        if (strExt.equals("png")) {
            resizeImagePNG(source, destino, width, height);
        } else {
            writeJpeg(bufferedScaled, dest.getCanonicalPath(), 1.0f);
        }
        return true;
    }

    /**
     * Write a JPEG file setting the compression quality.
     *
     * @param image a BufferedImage to be saved
     * @param destFile destination file (absolute or relative path)
     * @param quality a float between 0 and 1, where 1 means uncompressed.
     * @throws IOException in case of problems writing the file
     */
    private static void writeJpeg(BufferedImage image, String destFile, float quality)
            throws IOException {
        ImageWriter writer = null;
        FileImageOutputStream output = null;
        try {
            writer = ImageIO.getImageWritersByFormatName("jpeg").next();
            ImageWriteParam param = writer.getDefaultWriteParam();
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(quality);
            output = new FileImageOutputStream(new File(destFile));
            writer.setOutput(output);
            IIOImage iioImage = new IIOImage(image, null, null);
            writer.write(null, iioImage, param);
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (writer != null) {
                writer.dispose();
            }
            if (output != null) {
                output.close();
            }
        }
    }

    /**
     * Resize image for PNG file.
     *
     * @param sourceImg original image path
     * @param destImg destination file (absolute or relative path)
     * @param strExt extension file (absolute or relative path)
     * @param width for the width of image
     * @param height for the height of image
     */
    public static Boolean resizeImagePNG(String sourceImg, String destImg, Integer width, Integer height) {
        BufferedImage origImage;
        try {

            origImage = ImageIO.read(new File(sourceImg));
            int type = origImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : origImage.getType();

            //*Special* if the width or height is 0 use image src dimensions
            if (width == 0) {
                width = origImage.getWidth();
            }
            if (height == 0) {
                height = origImage.getHeight();
            }

            int fHeight = height;
            int fWidth = width;

            //Work out the resized width/height
            if (origImage.getHeight() > height || origImage.getWidth() > width) {
                fHeight = height;
                int wid = width;
                float sum = (float) origImage.getWidth() / (float) origImage.getHeight();
                fWidth = Math.round(fHeight * sum);

                if (fWidth > wid) {
                    //rezise again for the width this time
                    fHeight = Math.round(wid / sum);
                    fWidth = wid;
                }
            }

            BufferedImage resizedImage = new BufferedImage(fWidth, fHeight, type);
            Graphics2D g = resizedImage.createGraphics();
            g.setComposite(AlphaComposite.Src);

            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            g.drawImage(origImage, 0, 0, fWidth, fHeight, null);
            g.dispose();
            ImageIO.write(resizedImage, "png", new File(destImg));
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }
}
