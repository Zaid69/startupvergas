/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.HashMap;
import model.UsuarioModel;
import sql.HSql;

/**
 *
 * @author nochtli13
 */
public class UsuarioDao {
    
    public UsuarioModel getUsuarioByCNip(UsuarioModel um) {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM usuario WHERE usr_correo='"+um.getUsrCorreo()+"'"
                +" AND usr_nip='"+um.getUsrNip()+"' AND usr_status=1";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        UsuarioModel usrM = new UsuarioModel();
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            usrM.setUsrId(registro.get("usr_id").toString());
            usrM.setUsrCorreo(registro.get("usr_correo").toString());
            usrM.setUsrNombre(registro.get("usr_nombre").toString());
            usrM.setUsrStatus(registro.get("usr_status").toString());
        }
        return usrM;
    }
}
