/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.HashMap;
import model.HomeModel;
import sql.HSql;

/**
 *
 * @author nochtli13
 */
public class HomeDao {

    /* Se obtiene un HomeModel de acuerdo al homeId 
    que se recibe como parametro*/
    public HomeModel getHomeById(String homeId) {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM home WHERE home_id=" + homeId + "  AND home_status = 1";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        HomeModel homeModel = new HomeModel();
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            homeModel.setHomeId(registro.get("home_id").toString());
            homeModel.setHomeTitulo(registro.get("home_titulo").toString());
            homeModel.setHomeDesc(registro.get("home_desc").toString());
            homeModel.setHomeEncsecundario(registro.get("home_encsecundario").toString());
            homeModel.setHomeTsecundario(registro.get("home_tsecundario").toString());
            homeModel.setHomeSubsecundario(registro.get("home_subsecundario").toString());
            homeModel.setHomeDescsecundario(registro.get("home_descsecundario").toString());
            homeModel.setHomeStatus(registro.get("home_status").toString());
            homeModel.setHomeUpdate(registro.get("home_update").toString());
        }
        return homeModel;
    }

    /* Metodo que regresa un arreglo de HomeModel cuyo status sea 1*/
    public HomeModel[] getHomeModel() {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM home WHERE home_status = 1";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        HomeModel[] homeModels = new HomeModel[arrQuery.length];
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            HomeModel homeModel = new HomeModel();
            homeModel.setHomeId(registro.get("home_id").toString());
            homeModel.setHomeTitulo(registro.get("home_titulo").toString());
            homeModel.setHomeDesc(registro.get("home_desc").toString());
            homeModel.setHomeEncsecundario(registro.get("home_encsecundario").toString());
            homeModel.setHomeTsecundario(registro.get("home_tsecundario").toString());
            homeModel.setHomeSubsecundario(registro.get("home_subsecundario").toString());
            homeModel.setHomeDescsecundario(registro.get("home_descsecundario").toString());
            homeModel.setHomeStatus(registro.get("home_status").toString());
            homeModel.setHomeUpdate(registro.get("home_update").toString());
            homeModels[i] = homeModel;
        }
        return homeModels;
    }

    public boolean updateHomeModel(HomeModel hm) {
        boolean result = false;
        HSql sql = new HSql();
        String strSQL = "UPDATE home SET "+
                " home_titulo='"+hm.getHomeTitulo() +"',"+
                " home_desc='"+hm.getHomeDesc() +"',"+
                " home_encsecundario='"+hm.getHomeEncsecundario() +"',"+
                " home_tsecundario='"+hm.getHomeTsecundario() +"',"+
                " home_subsecundario='"+hm.getHomeSubsecundario()  +"',"+
                " home_descsecundario='"+hm.getHomeDescsecundario() +"',"+
                " home_update=now() "+
                " WHERE home_id="+hm.getHomeId();
        result = sql.ComandoConsola(strSQL);
        return result;
    }

}
