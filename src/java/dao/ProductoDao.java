/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import model.ProductoModel;
import org.apache.commons.io.FileUtils;
import sql.HSql;
import utilities.Tools;

/**
 *
 * @author nochtli13
 */
public class ProductoDao {

    public ProductoModel[] getProductos() {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM producto WHERE prod_status = 1";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        ProductoModel[] productoModels = new ProductoModel[arrQuery.length];
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            ProductoModel pM = new ProductoModel();
            pM.setProdId(registro.get("prod_id").toString());
            pM.setProdImg(registro.get("prod_img").toString());
            pM.setProdTitulo(registro.get("prod_titulo").toString());
            pM.setProdDesc(registro.get("prod_desc").toString());
            pM.setProdStatus(registro.get("prod_status").toString());
            pM.setProdUpdate(registro.get("prod_update").toString());
            productoModels[i] = pM;
        }
        return productoModels;
    }

    public ProductoModel getProdById(String prodId) {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM producto WHERE prod_id=" + prodId + "  AND prod_status = 1";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        ProductoModel pM = new ProductoModel();
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            pM.setProdId(registro.get("prod_id").toString());
            pM.setProdImg(registro.get("prod_img").toString());
            pM.setProdTitulo(registro.get("prod_titulo").toString());
            pM.setProdDesc(registro.get("prod_desc").toString());
            pM.setProdStatus(registro.get("prod_status").toString());
            pM.setProdUpdate(registro.get("prod_update").toString());
        }
        return pM;
    }

    public boolean updateProducto(ProductoModel pM) {
        HSql sql = new HSql();
        String strSQL = "UPDATE producto SET "
                + " prod_titulo = '" + pM.getProdTitulo() + "',"
                + " prod_desc = '" + pM.getProdDesc() + "',"
                + " prod_update = now() "
                + " WHERE prod_id = " + pM.getProdId();
        return sql.ComandoConsola(strSQL);
    }

public String addProducto(ProductoModel pm) {
        HSql sql = new HSql();
        String idProducto = "";
        HashMap[] arrResultados;
        String strSQL="";
        strSQL = "INSERT INTO `startup`.`producto` "
                + "(`prod_img`, `prod_titulo`, `prod_desc`, `prod_status`, `prod_update`)"
                + " VALUES ('"+pm.getProdImg()+"', '"+pm.getProdTitulo()+"', '"+pm.getProdDesc()+"', '1', now())";
        arrResultados = sql.ExecuteSQLWithReturn(strSQL, "producto");
        HashMap resultado = (HashMap) arrResultados[0];
        idProducto = (String) resultado.get("last_id");
        return idProducto;
    }

    public boolean cargarProducto(String strIdr, String strFileUploaded, String strRepository, String strExt, String archivoBorrar) throws IOException {
        String strRutaImagen = "";
        String strSeparadorCarpeta = "\\";
        if (strFileUploaded.lastIndexOf("\\") > 0) {
            strSeparadorCarpeta = "\\";
        } else {
            strSeparadorCarpeta = "/";
        }
        boolean success = false;
        String repository = strRepository + strSeparadorCarpeta + "media";
        String strPicturesRepository = repository;
        strRutaImagen = strPicturesRepository;
        File f = new File(strRutaImagen);
        if (f.isDirectory() == false) {
            success = f.mkdir();
        }
        strRutaImagen = strPicturesRepository + strSeparadorCarpeta + "producto";
        f = new File(strRutaImagen);
        if (f.isDirectory() == false) {
            success = f.mkdir();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String strFecha = sdf.format(new Date());
        strRutaImagen = strRutaImagen + strSeparadorCarpeta + strIdr + "_" + strFecha + "." + strExt;
        String prod_foto = "media/producto/" + strIdr + "_" + strFecha + "." + strExt;
        File uploadedFile = new File(strFileUploaded);
        Tools.resizeImagenComplete(strFileUploaded, strRutaImagen, strExt, 95, 80);

        //borra el que subio en el post "upload"
        uploadedFile.delete();
        if (archivoBorrar.length() > 0) {
            uploadedFile = new File(archivoBorrar);
            uploadedFile.delete();
        }
        HSql sql = new HSql();
        String strSQL = "UPDATE producto SET ";
        strSQL = strSQL + " prod_img = '" + prod_foto + "',";
        strSQL = strSQL + " prod_update = now() ";
        strSQL = strSQL + " WHERE prod_id = " + strIdr;
        return sql.ComandoConsola(strSQL);
    }

}
