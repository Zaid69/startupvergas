/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HomeDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.HomeModel;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author nochtli13
 */
public class HomeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String jsonString = "";
        JSONParser parser = new JSONParser();
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
            System.out.println("Form tipo multipart");
        }
        try {
            String apiCall = request.getParameter("ApiCall");
            if (null != request.getParameter("ApiCall")) {
                try {
                    if (apiCall.equals("editHome")) {
                        HomeModel hm = new HomeModel();
                        hm.setHomeId(request.getParameter("idHome"));
                        hm.setHomeTitulo(request.getParameter("hTitulo"));
                        hm.setHomeDesc(request.getParameter("hDesc").trim());
                        hm.setHomeEncsecundario(request.getParameter("hEncSec"));
                        hm.setHomeTsecundario(request.getParameter("hTSec"));
                        hm.setHomeSubsecundario(request.getParameter("hSubSec"));
                        hm.setHomeDescsecundario(request.getParameter("hDescSec").trim());
                        boolean resEdit = new HomeDao().updateHomeModel(hm);
                        response.sendRedirect("dashboard/producto_detalle.jsp?modelId=" + hm.getHomeId() + "&res=" + resEdit);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /*Regresa un arreglo de HomeModels */
    public HomeModel[] getHomeModels() {
        HomeModel[] homeModels = new HomeDao().getHomeModel();
        return homeModels;
    }

    /* Regresa un solo modelo acorde al Id*/
    public HomeModel getModelById(String homeId) {
        HomeModel homeModel = new HomeDao().getHomeById(homeId);
        return homeModel;
    }
}
