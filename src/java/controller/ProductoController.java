/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.oreilly.servlet.MultipartRequest;
import dao.ProductoDao;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ProductoModel;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author nochtli13
 */
public class ProductoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        String strRespuesta="";
        if (isMultipart) {
            strRespuesta = UploadDoc(request);
            //ReadImageDropZone(request, response);
            response.sendRedirect(strRespuesta);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

 
    public ProductoModel getProductoById(String prodId){
      return new ProductoDao().getProdById(prodId);
    }
    
    public ProductoModel[] getProductoModels(){
      return new ProductoDao().getProductos();
    }
    
    private String UploadDoc(HttpServletRequest request) throws IOException {
        ServletContext context = getServletContext();
        boolean success = false;
        boolean hasfile = false;
        String strRuta = context.getRealPath(request.getServletPath());
        String strRutaProd = context.getRealPath(request.getServletPath());
        String strRutaFinal = "";
        if (strRuta.lastIndexOf("\\") > 0) {
            strRuta = strRuta.substring(0, strRuta.lastIndexOf("\\"));
            strRutaFinal = strRuta;
            strRuta += "\\upload\\";
        } else {
            strRuta = strRuta.substring(0, strRuta.lastIndexOf("/"));
            strRutaFinal = strRuta;
            strRuta += "/upload/";
        }
        File f = new File(strRuta);
        if (f.isDirectory() == false) {
            success = f.mkdir();
        }
        //Ejecuta métodos establece el tamanio maximo de la imagen que puedo recibir
        MultipartRequest mRequest = new MultipartRequest(request, strRuta, 1150000);
        String hidCallback = "";
        String apiCall = mRequest.getParameter("APICall");

        Enumeration objArchivos = mRequest.getFileNames();
        String strArchivo = "";
        String strArchivoO = "";
        String strExt = "";
        boolean blnEstatus = false;
        String error="0";
        while (objArchivos.hasMoreElements()) {
            strArchivo = (String) objArchivos.nextElement();
            strArchivoO = mRequest.getOriginalFileName(strArchivo);
            strExt = FilenameUtils.getExtension(strArchivoO);
            if (strArchivoO != null) {
                hasfile = true;
            }
        }//while

        String strFileUploaded = "";
        if (strArchivoO != null) {
            if (strRuta.lastIndexOf("\\") > 0) {
                strFileUploaded = strRuta + strArchivoO;
            } else {
                strFileUploaded = strRuta + strArchivoO;
            }
        }

        String idProd = mRequest.getParameter("idProd");
        if (idProd == null) {
            idProd = "";
        }
        String txtTitulo = new String(mRequest.getParameter("txtTitulo").getBytes("iso-8859-1"), "UTF-8").trim();
        String txtDesc = new String(mRequest.getParameter("txtDesc").getBytes("iso-8859-1"), "UTF-8").trim();
       
        //String docId = mRequest.getParameter("txtDoc");
        ProductoModel pm = new ProductoModel();
        pm.setProdId(idProd);
        pm.setProdTitulo(txtTitulo);
        pm.setProdDesc(txtDesc);
//        ESTAS PARTES QUE ESTAN COMENTADAS ES PORQUE NO TENIA IMG ME BOTABA ERROR DESCOMENTAR DESPUES
        ProductoModel  pmConImg=new ProductoDao().getProdById(idProd);
      
//        IF PARA EDITAR PRIDUCTO
        if (apiCall.equalsIgnoreCase("editaProd")) {
            blnEstatus=new ProductoDao().updateProducto(pm);
            String archivoBorrar="";
            if (strRutaProd.lastIndexOf("\\") > 0) {
                strRutaProd = strRutaProd.substring(0, strRutaProd.lastIndexOf("\\"));
                strRutaProd += "\\";
//        ESTAS PARTES QUE ESTAN COMENTADAS ES PORQUE NO TENIA IMG ME BOTABA ERROR DESCOMENTAR DESPUES
                archivoBorrar = strRutaProd + pmConImg.getProdImg();
                archivoBorrar=archivoBorrar.replaceAll("\\","\\\\");
            } else {
                strRutaProd = strRutaProd.substring(0, strRutaProd.lastIndexOf("/"));
                strRutaProd += "/";
                
                archivoBorrar = strRutaProd + pmConImg.getProdImg();
            }
            if (hasfile) {
                blnEstatus = new ProductoDao().cargarProducto(idProd, strFileUploaded, strRutaFinal, strExt, archivoBorrar);
            }
            
            hidCallback ="dashboard/producto_detalle.jsp?productoId="+idProd;
            
        }
//        IF PARA AGEGAR PRODUCTO NUEVO
        if (apiCall.equalsIgnoreCase("nuevoProd")) {
            
            pm.setProdTitulo(txtTitulo);
            pm.setProdDesc(txtDesc);
            
            idProd = new ProductoDao().addProducto(pm);
            String archivoBorrar = "";
            if (strRutaProd.lastIndexOf("\\") > 0) {
                strRutaProd = strRutaProd.substring(0, strRutaProd.lastIndexOf("\\"));
                strRutaProd += "\\";
            } else {
                strRutaProd = strRutaProd.substring(0, strRutaProd.lastIndexOf("/"));
                strRutaProd += "/";
            }
            if (hasfile) {
                blnEstatus = new ProductoDao().cargarProducto(idProd, strFileUploaded, strRutaFinal, strExt, archivoBorrar);
            }
            hidCallback = "dashboard/producto_detalle.jsp?productoId=" + idProd;
        }
        return hidCallback;
    }
    
}
