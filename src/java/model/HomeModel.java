package model;
/**
 *
 * @author nochtli13
 */
public class HomeModel {
String homeId;
String homeTitulo;
String homeDesc;
String homeEncsecundario;
String homeTsecundario;
String homeSubsecundario;
String homeDescsecundario;
String homeStatus;
String homeUpdate;

    public HomeModel() {
        this.homeId = "";
        this.homeTitulo = "";
        this.homeDesc = "";
        this.homeEncsecundario = "";
        this.homeTsecundario = "";
        this.homeSubsecundario = "";
        this.homeDescsecundario = "";
        this.homeStatus = "";
        this.homeUpdate = "";
    }

    public void setHomeId(String homeId) {
        this.homeId = homeId;
    }

    public void setHomeTitulo(String homeTitulo) {
        this.homeTitulo = homeTitulo;
    }

    public void setHomeDesc(String homeDesc) {
        this.homeDesc = homeDesc;
    }

    public void setHomeEncsecundario(String homeEncsecundario) {
        this.homeEncsecundario = homeEncsecundario;
    }

    public void setHomeTsecundario(String homeTsecundario) {
        this.homeTsecundario = homeTsecundario;
    }

    public void setHomeSubsecundario(String homeSubsecundario) {
        this.homeSubsecundario = homeSubsecundario;
    }

    public void setHomeDescsecundario(String homeDescsecundario) {
        this.homeDescsecundario = homeDescsecundario;
    }

    public void setHomeStatus(String homeStatus) {
        this.homeStatus = homeStatus;
    }

    public void setHomeUpdate(String homeUpdate) {
        this.homeUpdate = homeUpdate;
    }

    public String getHomeId() {
        return homeId;
    }

    public String getHomeTitulo() {
        return homeTitulo;
    }

    public String getHomeDesc() {
        return homeDesc;
    }

    public String getHomeEncsecundario() {
        return homeEncsecundario;
    }

    public String getHomeTsecundario() {
        return homeTsecundario;
    }

    public String getHomeSubsecundario() {
        return homeSubsecundario;
    }

    public String getHomeDescsecundario() {
        return homeDescsecundario;
    }

    public String getHomeStatus() {
        return homeStatus;
    }

    public String getHomeUpdate() {
        return homeUpdate;
    }



}
