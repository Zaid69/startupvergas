/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author nochtli13
 */
public class ProductoModel {
String prodId;
String prodImg;
String prodTitulo;
String prodDesc;
String prodStatus;
String prodUpdate;

    public ProductoModel() {
        this.prodId = "";
        this.prodImg = "";
        this.prodTitulo = "";
        this.prodDesc = "";
        this.prodStatus = "";
        this.prodUpdate = "";
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public void setProdImg(String prodImg) {
        this.prodImg = prodImg;
    }

    public void setProdTitulo(String prodTitulo) {
        this.prodTitulo = prodTitulo;
    }

    public void setProdDesc(String prodDesc) {
        this.prodDesc = prodDesc;
    }

    public void setProdStatus(String prodStatus) {
        this.prodStatus = prodStatus;
    }

    public void setProdUpdate(String prodUpdate) {
        this.prodUpdate = prodUpdate;
    }

    public String getProdId() {
        return prodId;
    }

    public String getProdImg() {
        return prodImg;
    }

    public String getProdTitulo() {
        return prodTitulo;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public String getProdStatus() {
        return prodStatus;
    }

    public String getProdUpdate() {
        return prodUpdate;
    }



}
