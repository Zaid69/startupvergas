/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author nochtli13
 */
public class UsuarioModel {
String usrId;
String usrNombre;
String usrCorreo;
String usrNip;
String usrStatus;
String usrUpdate;

    public UsuarioModel() {
        this.usrId = "";
        this.usrNombre = "";
        this.usrCorreo = "";
        this.usrNip = "";
        this.usrStatus = "";
        this.usrUpdate = "";
    }

    public String getUsrId() {
        return usrId;
    }

    public String getUsrNombre() {
        return usrNombre;
    }

    public String getUsrCorreo() {
        return usrCorreo;
    }

    public String getUsrNip() {
        return usrNip;
    }

    public String getUsrStatus() {
        return usrStatus;
    }

    public String getUsrUpdate() {
        return usrUpdate;
    }

    public void setUsrId(String usrId) {
        this.usrId = usrId;
    }

    public void setUsrNombre(String usrNombre) {
        this.usrNombre = usrNombre;
    }

    public void setUsrCorreo(String usrCorreo) {
        this.usrCorreo = usrCorreo;
    }

    public void setUsrNip(String usrNip) {
        this.usrNip = usrNip;
    }

    public void setUsrStatus(String usrStatus) {
        this.usrStatus = usrStatus;
    }

    public void setUsrUpdate(String usrUpdate) {
        this.usrUpdate = usrUpdate;
    }
    



}
