<%-- 
    Document   : paquetes
    Created on : Jul 13, 2019, 8:05:36 AM
    Author     : nochtli13
--%>

<%@page import="controller.ProductoController"%>
<%@page import="model.ProductoModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        //Aqui va el arreglo de modelos
      ProductoModel[] prodModels = new ProductoController().getProductoModels();
    %>
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="codepixer">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>Startup</title>

        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
        <!--
        CSS
        ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/nice-select.css">					
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="js/utilities/imports.js"></script>
    </head>
    <body>
        <header id="header"></header>

        <!-- Start we-offer Area -->
        <section class="we-offer-area section-gap" id="offer">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-60 col-lg-10">
                        <div class="title text-center">
                            <h1 class="mb-10">Our Offered Services</h1>
                            <p>Who are in extremely love with eco friendly system.</p>
                        </div>
                    </div>
                </div>						
                <div class="row">
                    <!-- Colocarlo dentro de un for -->
                    <%for(int cProd=0;cProd<prodModels.length;cProd++){ 
                    ProductoModel pm=prodModels[cProd];
                    %>
                    <div class="col-lg-6">
                        <div class="single-offer d-flex flex-row pb-30">
                            <div class="icon">
                                <img src="<%=pm.getProdImg()%>" alt="">
                            </div>
                            <div class="desc">
                                <a href="#"><h4><%=pm.getProdTitulo()%></h4></a>
                                <p><%=pm.getProdDesc()%></p>
                            </div>
                        </div>
                    </div>
                    <%}%>
                    <!-- End for -->
                </div>
            </div>	
        </section>
        <!-- End we-offer Area -->


        <!-- Start home-video Area -->
        <section class="home-video-area" id="about">
            <div class="container-fluid">
                <div class="row justify-content-end align-items-center">
                    <div class="col-lg-4 no-padding video-right">
                        <p class="top-title">Tutorial for beginner</p>
                        <h1>Watch tutorial <br>
                            Video of SaaS to start</h1>
                        <p><span>We are here to listen from you deliver exellence</span></p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore. magna aliqua. Ut enim ad minim. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore. magna aliqua. Ut enim ad minim.
                        </p>
                    </div>
                    <section class="video-area col-lg-6">
                        <div class="overlay overlay-bg"></div>
                        <div class="container">
                            <div class="video-content">
                                <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="play-btn"><img src="img/play-btn.png" alt=""></a>
                            </div>
                        </div>
                    </section>											
                </div>
            </div>	
        </section>
        <!-- End home-aboutus Area -->

        <!-- start footer Area -->		
        <footer id="footer"></footer>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>			
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
        <script src="js/easing.min.js"></script>			
        <script src="js/hoverIntent.js"></script>
        <script src="js/superfish.min.js"></script>	
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>	
        <script src="js/owl.carousel.min.js"></script>			
        <script src="js/jquery.sticky.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>			
        <script src="js/parallax.min.js"></script>		
        <script src="js/mail-script.js"></script>	
        <script src="js/main.js"></script>	
    </body>
</html>
