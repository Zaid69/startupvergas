<%-- 
    Document   : producto_lista
    Created on : Jul 15, 2019, 2:02:57 PM
    Author     : nochtli13
--%>

<%@page import="model.HomeModel"%>
<%@page import="controller.HomeController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <%
        HomeModel[] homeModels = new HomeController().getHomeModels();
    %>
    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="au theme template">
        <meta name="author" content="Hau Nguyen">
        <meta name="keywords" content="au theme template">

        <!-- Title Page-->
        <title>Listado</title>

        <!-- Fontfaces CSS-->
        <link href="css/font-face.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

        <!-- Vendor CSS-->
        <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
        <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

        <!-- Main CSS-->
        <link href="css/theme.css" rel="stylesheet" media="all">
        <link href="css/estilos.css" rel="stylesheet" media="all">

        <link href="images/fav_icon.png" rel="shortcut icon" type="image/x-icon">
        <link href="images/fav_icon.png" rel="apple-touch-icon">
        <!-- Jquery JS-->
        <script src="vendor/jquery-3.2.1.min.js"></script>
        <!-- For personal functions-->
        <script src="js/home.js"></script>
        <script src="js/general.js"></script>
        <script src="js/imports.js"></script>
    </head>

    <body class="animsition">
        <div class="page-wrapper">
            <!-- HEADER MOBILE-->
            <%@include file="header_mobile.jsp"%>
            <!-- END HEADER MOBILE-->
            <!-- MENU SIDEBAR-->
            <%@include file="menu_slider.jsp"%>
            <!-- END MENU SIDEBAR-->

            <!-- PAGE CONTAINER-->
            <div class="page-container">
                <!-- HEADER DESKTOP-->
                <%@include file="header_desk.jsp"%>
                <!-- HEADER DESKTOP-->
                <!-- MAIN CONTENT-->
                <div class="main-content margen_sup_titulo">
                    <div class="section__content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- DATA TABLE-->
                                    <div class="table-responsive m-b-40">
                                        <table id="tbHome" class="table table-borderless table-data3">
                                            <thead>
                                                <tr>
                                                    <th>Id Home</th>
                                                    <th>Titulo 1</th>
                                                    <th>Desc 1</th>
                                                    <th>Titulo 2</th>
                                                    <th>Subtitulo 2</th>
                                                    <th>SubSubtitulo 2</th>
                                                    <th>SubSubDesc 2</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%for (int cCout = 0; cCout < homeModels.length; cCout++) {
                                                        HomeModel hm = homeModels[cCout];
                                                %>
                                                <tr data-id="<%=hm.getHomeId()%>" >
                                                    <td><%=hm.getHomeId()%></td>
                                                    <td><%=hm.getHomeTitulo()%></td>
                                                    <td><%=hm.getHomeDesc()%></td>
                                                    <td><%=hm.getHomeEncsecundario()%> </td>
                                                    <td><%=hm.getHomeTsecundario()%></td>
                                                    <td><%=hm.getHomeSubsecundario()%></td>
                                                    <td><%=hm.getHomeDescsecundario()%></td>
                                                </tr>
                                                <%}%>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- END DATA TABLE-->
                                </div>
                            </div>
                            <!-- FOOTER-->
                            <div id="footer"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        
        <!-- Bootstrap JS-->
        <script src="vendor/bootstrap-4.1/popper.min.js"></script>
        <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
        <!-- Vendor JS       -->
        <script src="vendor/slick/slick.min.js">
        </script>
        <script src="vendor/wow/wow.min.js"></script>
        <script src="vendor/animsition/animsition.min.js"></script>
        <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
        </script>
        <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
        <script src="vendor/counter-up/jquery.counterup.min.js">
        </script>
        <script src="vendor/circle-progress/circle-progress.min.js"></script>
        <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
        <script src="vendor/chartjs/Chart.bundle.min.js"></script>
        <script src="vendor/select2/select2.min.js">
        </script>

        <!-- Main JS-->
        <script src="js/main.js"></script>

        <!-- modal medium -->
        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Sistema Start up</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Textooooooo
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->
    </body>

</html>
<!-- end document-->

