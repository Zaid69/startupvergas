<%-- 
    Document   : menu_slider
    Created on : Jul 22, 2019, 10:17:46 PM
    Author     : nochtli13
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/upem.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub active">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-barcode"></i>Products</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="producto_lista.jsp">Listado</a>
                                </li>
                                <li>
                                    <a href="producto_detalle.jsp?modelId=1">Editar</a>
                                    <!--<a href="producto_detalle.jsp?modelId=1">Editar</a>-->
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-home"></i>Home</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="home_lista.jsp">Listado</a>
                                </li>
                                <li>
                                    <a href="home_detalle.jsp">Editar</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tasks"></i>Proyects</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="producto_lista.jsp">Listado</a>
                                </li>
                                <li>
                                    <a href="producto_detalle.jsp">Nuevo</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-user"></i>About Us</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="producto_lista.jsp">Listado</a>
                                </li>
                                <li>
                                    <a href="producto_detalle.jsp">Nuevo</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-box"></i>Package</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="producto_lista.jsp">Listado</a>
                                </li>
                                <li>
                                    <a href="producto_detalle.jsp">Nuevo</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
    </body>
</html>
