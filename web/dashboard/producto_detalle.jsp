<%-- 
    Document   : producto_detalle
    Created on : Jul 15, 2019, 2:03:41 PM
    Author     : nochtli13
--%>
<%@page import="controller.ProductoController"%>
<%@page import="model.ProductoModel"%>
<%@page import="controller.HomeController"%>
<%@page import="model.HomeModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <%
        String productoId = request.getParameter("productoId");
        if(productoId==null){
            productoId="";
            System.out.println(""+productoId);
        }
        ProductoModel prodModel = new ProductoController().getProductoById(productoId);

    %>
    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="au theme template">
        <meta name="author" content="Hau Nguyen">
        <meta name="keywords" content="au theme template">

        <!-- Title Page-->
        <title>Detalle</title>

        <!-- Fontfaces CSS-->
        <link href="css/font-face.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

        <!-- Vendor CSS-->
        <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
        <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

        <!-- Main CSS-->
        <link href="css/theme.css" rel="stylesheet" media="all">
        <link href="css/estilos.css" rel="stylesheet" media="all">

        <link href="images/fav_icon.png" rel="shortcut icon" type="image/x-icon">
        <link href="images/fav_icon.png" rel="apple-touch-icon">

        <!-- Jquery JS-->
        <script src="vendor/jquery-3.2.1.min.js"></script>
        <!-- For personal functions-->
        <script src="js/productos.js"></script>
        <script src="js/imports.js"></script>
    </head>
    <body class="animsition">
        <div class="page-wrapper">
            <!-- HEADER MOBILE-->
            <%@include file="header_mobile.jsp"%>
            <!-- END HEADER MOBILE-->
            <!-- MENU SIDEBAR-->
            <%@include file="menu_slider.jsp"%>
            <!-- END MENU SIDEBAR-->
            <!-- PAGE CONTAINER-->
            <div class="page-container">
                <!-- HEADER DESKTOP-->
                <%@include file="header_desk.jsp"%>
                <!-- HEADER DESKTOP-->
                <!-- MAIN CONTENT-->
                <div class="main-content">
                    <div class="section__content section__content--p30">
                        <div class="container-fluid">
                            <div class="row">
                                <!-- Form basic-->
                                <div class="col-lg-12 margen_sup_tit">
                                    <form action="../ProductoController" method="post" id="frmUpdateProd" 
                                          class="form-horizontal" enctype="multipart/form-data" >
                                        <div class="card">
                                            <div class="card-header">
                                                <%if(productoId.equals("")){%>
                                                    <strong>Nuevo </strong> Producto
                                                <%}else{%>
                                                    <strong>Edición </strong> Producto
                                                <%}%>
                                            </div>
                                            <div class="card-body card-block">

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="text-input" class=" form-control-label">Imagen</label>
                                                    </div>
                                                     <%if(productoId.equals("")){%>
                                                   <div class="col-12 col-md-9">
                                                        <img src="../img/rnd.png" width="60px">
                                                            <div class="box">
                                                            <input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1" 
                                                                   data-multiple-caption="{count} files selected" style="display: none;"/>
                                                            <label for="file-1">
                                                                <span>Choose a product file</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-file-image"></i>
                                                            </label>
                                                   </div>
                                                   </div>     
                                                <%}else{%>
                                                      <div class="col-12 col-md-9">
                                                        <img src="../<%=prodModel.getProdImg()%>" width="60px"/>
                                                        <!-- For input mask-->
                                                        <div class="box">
                                                            <input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1" 
                                                                   data-multiple-caption="{count} files selected" style="display: none;"/>
                                                            <label for="file-1">
                                                                <span>Choose a product file</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-file-image"></i>
                                                            </label>
                                                        </div>
                                                        <!-- End input mask-->
                                                    </div>
                                                <%}%>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="text-input" class=" form-control-label">Título</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="text-input" name="txtTitulo" 
                                                               value="<%=prodModel.getProdTitulo()%>"
                                                               placeholder="Text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="textarea-input" class=" form-control-label">Descripción</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <textarea name="txtDesc" id="textarea-input" 
                                                                  rows="9" placeholder="Content..." class="form-control">
                                                            <%=prodModel.getProdDesc()%>
                                                        </textarea>
                                                    </div>
                                                </div>

                                                        
                                               <%if(productoId.equals("")){%>
                                                <div class="card-footer">
                                                    
                                                    <input type="hidden" name="APICall" value="nuevoProd">
                                                    <button type="reset" class="btn btn-danger btn-sm boton_gral"  data-toggle="modal" data-target="#staticModal">
                                                        <i class="fa fa-ban"></i> Cancel
                                                    </button>
                                                    <button type="reset" class="btn btn-success btn-sm boton_gral" onclick="updateProducto()">
                                                        <i class="fa fa-dot-circle-o"></i> Crear
                                                    </button>
                                                 </div>   
                                                <%}else{%>
                                                    <div class="card-footer">
                                                    <input type="hidden" name="idProd" value="<%=prodModel.getProdId()%>">
                                                    <input type="hidden" name="APICall" value="editaProd">
                                                    <button type="reset" class="btn btn-danger btn-sm boton_gral"  data-toggle="modal" data-target="#staticModal">
                                                        <i class="fa fa-ban"></i> Cancel
                                                    </button>
                                                    <button type="reset" class="btn btn-success btn-sm boton_gral" onclick="updateProducto()">
                                                        <i class="fa fa-dot-circle-o"></i> Guardar Cambios
                                                    </button>
                                                     </div>   
                                                    <%}%>
                                                
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- Form basic-->
                            </div>
                            <!-- FOOTER-->
                            <div id="footer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap JS-->
        <script src="vendor/bootstrap-4.1/popper.min.js"></script>
        <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
        <!-- Vendor JS       -->
        <script src="vendor/slick/slick.min.js">
        </script>
        <script src="vendor/wow/wow.min.js"></script>
        <script src="vendor/animsition/animsition.min.js"></script>
        <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
        </script>
        <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
        <script src="vendor/counter-up/jquery.counterup.min.js">
        </script>
        <script src="vendor/circle-progress/circle-progress.min.js"></script>
        <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
        <script src="vendor/chartjs/Chart.bundle.min.js"></script>
        <script src="vendor/select2/select2.min.js">
        </script>

        <!-- Main JS-->
        <script src="js/main.js"></script>

        <!-- For input mask-->
        <script src="js/custom-file-input.js"></script>

        <!-- modal medium -->
        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Sistema Start up</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Favor de escribir los datos requeridos
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal medium -->
        <!-- modal static -->
        <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
             data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticModalLabel">Static Modal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            This is a static modal, backdrop click will not close it.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal static -->
    </body>

</html>
<!-- end document-->
