<%-- 
    Document   : header_desk
    Created on : Jul 22, 2019, 10:16:26 PM
    Author     : nochtli13
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <header class="header-desktop">
                        <div class="pull-right margen_sup">
                            <div class="container-fluid">
                                <div class="account-item clearfix js-item-menu">
                                    <div class="image">
                                        <img src="images/icon/baseline_person.png" alt="Usuario" />
                                    </div>
                                    <div class="content">
                                        <a class="js-acc-btn" href="#">Usuario</a>
                                    </div>
                                    <div class="account-dropdown js-dropdown">
                                        <div class="info clearfix">
                                            <div class="image">
                                                <a href="#">
                                                    <img src="images/icon/baseline_person.png" alt="Usuario" />
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h5 class="name">
                                                    <a href="#">Usuario</a>
                                                </h5>
                                                <span class="email"><%=session.getAttribute("usuarioName")%></span>
                                            </div>
                                        </div>                                                
                                        <div class="account-dropdown__footer">
                                            <a href="../Logout">
                                                <i class="zmdi zmdi-power"></i>Logout</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
    </body>
</html>
