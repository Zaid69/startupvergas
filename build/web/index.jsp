<%-- 
    Document   : index
    Created on : Jul 13, 2019, 7:37:07 AM
    Author     : nochtli13
--%>

<%@page import="controller.HomeController"%>
<%@page import="model.HomeModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <%
    HomeModel homeModel=new HomeController().getModelById("1");
%>
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="codepixer">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>Startup</title>

        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
        <!--
        CSS
        ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/nice-select.css">					
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="js/utilities/imports.js"></script>
        
        <link href="img/fav_icon.png" rel="shortcut icon" type="image/x-icon">
        <link href="img/fav_icon.png" rel="apple-touch-icon">
    </head>
    <body>
        <header id="header"></header>
        <!-- start banner Area -->
        <section class="banner-area" id="home">	
            <div class="container">
                <div class="row fullscreen d-flex align-items-center justify-content-center">
                    <div class="banner-content col-lg-7">
                        <h1>
                            <%=homeModel.getHomeTitulo()%>				
                        </h1>
                        <p class="pt-20 pb-20">
                            <%=homeModel.getHomeDesc()%>
                        </p>
                    </div>											
                </div>
            </div>
        </section>
        <!-- End banner Area -->	



        <!-- Start home-aboutus Area -->
        <section class="home-aboutus-area">
            <div class="container-fluid">
                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-8 no-padding about-left">
                        <img class="img-fluid" src="img/about-bg.jpg" alt="">
                    </div>
                    <div class="col-lg-4 no-padding about-right">
                        <p class="top-title"><%=homeModel.getHomeEncsecundario()%></p>
                        <h1 class="text-white"><%=homeModel.getHomeTsecundario()%></h1>
                        <p><span><%=homeModel.getHomeSubsecundario()%> </span></p>
                        <p>
                         <%=homeModel.getHomeDescsecundario()%>
                        </p>
                    </div>
                </div>
            </div>	
        </section>
        <!-- End home-aboutus Area -->


        <!-- Start protfolio Area -->
        <section class="protfolio-area section-gap" id="project">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-60 col-lg-10">
                        <div class="title text-center">
                            <h1 class="mb-10">Our Offered Services</h1>
                            <p>Who are in extremely love with eco friendly system.</p>
                        </div>
                    </div>
                </div>						
                <div class="row">
                    <div class="col-lg-8 single-portfolio">
                        <img class="image img-fluid" src="img/p1.jpg" alt="">
                        <a href="img/p1.jpg" class="img-pop-up">	
                            <div class="middle">
                                <div class="text"><span class="lnr lnr-frame-expand"></span></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 single-portfolio">
                        <img class="image img-fluid" src="img/p2.jpg" alt="">
                        <a href="img/p2.jpg" class="img-pop-up">	
                            <div class="middle">
                                <div class="text"><span class="lnr lnr-frame-expand"></span></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 single-portfolio">
                        <img class="image img-fluid" src="img/p3.jpg" alt="">
                        <a href="img/p3.jpg" class="img-pop-up">	
                            <div class="middle">
                                <div class="text"><span class="lnr lnr-frame-expand"></span></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-8 single-portfolio">
                        <img class="image img-fluid" src="img/p4.jpg" alt="">
                        <a href="img/p4.jpg" class="img-pop-up">	
                            <div class="middle">
                                <div class="text"><span class="lnr lnr-frame-expand"></span></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 single-portfolio">
                        <img class="image img-fluid" src="img/p5.jpg" alt="">
                        <a href="img/p5.jpg" class="img-pop-up">	
                            <div class="middle">
                                <div class="text"><span class="lnr lnr-frame-expand"></span></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 single-portfolio">
                        <img class="image img-fluid" src="img/p6.jpg" alt="">
                        <a href="img/p6.jpg" class="img-pop-up">	
                            <div class="middle">
                                <div class="text"><span class="lnr lnr-frame-expand"></span></div>
                            </div>
                        </a>
                    </div>						
                </div>
            </div>	
        </section>
        <!-- End protfolio Area -->			

        <!-- Start callto-action Area -->
        <section class="callto-action-area relative section-gap">
            <div class="overlay overlay-bg"></div>	
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content col-lg-9">
                        <div class="title text-center">
                            <h1 class="mb-10 text-white">Got Impressed to our features</h1>
                            <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                            <a class="primary-btn" href="#">Request Free Demo</a>
                        </div>
                    </div>
                </div>	
            </div>	
        </section>
        <!-- End calto-action Area -->

        <!-- Start testomial Area -->
        <section class="testomial-area section-gap">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-60 col-lg-8">
                        <div class="title text-center">
                            <h1 class="mb-10">Testimonial from our Clients</h1>
                            <p>Who are in extremely love with eco friendly system.</p>
                        </div>
                    </div>
                </div>						
                <div class="row">
                    <div class="active-tstimonial-carusel">
                        <div class="single-testimonial item">
                            <img class="mx-auto" src="img/t1.png" alt="">
                            <p class="desc">
                                Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
                            </p>
                            <h4>Mark Alviro Wiens</h4>
                            <p>
                                CEO at Google
                            </p>
                        </div>
                        <div class="single-testimonial item">
                            <img class="mx-auto" src="img/t2.png" alt="">
                            <p class="desc">
                                Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
                            </p>
                            <h4>Mark Alviro Wiens</h4>
                            <p>
                                CEO at Google
                            </p>
                        </div>
                        <div class="single-testimonial item">
                            <img class="mx-auto" src="img/t3.png" alt="">
                            <p class="desc">
                                Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
                            </p>
                            <h4>Mark Alviro Wiens</h4>
                            <p>
                                CEO at Google
                            </p>
                        </div>	
                        <div class="single-testimonial item">
                            <img class="mx-auto" src="img/t1.png" alt="">
                            <p class="desc">
                                Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
                            </p>
                            <h4>Mark Alviro Wiens</h4>
                            <p>
                                CEO at Google
                            </p>
                        </div>
                        <div class="single-testimonial item">
                            <img class="mx-auto" src="img/t2.png" alt="">
                            <p class="desc">
                                Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
                            </p>
                            <h4>Mark Alviro Wiens</h4>
                            <p>
                                CEO at Google
                            </p>
                        </div>
                        <div class="single-testimonial item">
                            <img class="mx-auto" src="img/t3.png" alt="">
                            <p class="desc">
                                Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
                            </p>
                            <h4>Mark Alviro Wiens</h4>
                            <p>
                                CEO at Google
                            </p>
                        </div>															
                        <div class="single-testimonial item">
                            <img class="mx-auto" src="img/t1.png" alt="">
                            <p class="desc">
                                Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
                            </p>
                            <h4>Mark Alviro Wiens</h4>
                            <p>
                                CEO at Google
                            </p>
                        </div>
                        <div class="single-testimonial item">
                            <img class="mx-auto" src="img/t2.png" alt="">
                            <p class="desc">
                                Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
                            </p>
                            <h4>Mark Alviro Wiens</h4>
                            <p>
                                CEO at Google
                            </p>
                        </div>
                        <div class="single-testimonial item">
                            <img class="mx-auto" src="img/t3.png" alt="">
                            <p class="desc">
                                Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
                            </p>
                            <h4>Mark Alviro Wiens</h4>
                            <p>
                                CEO at Google
                            </p>
                        </div>														
                    </div>
                </div>
            </div>	
        </section>
        <!-- End testomial Area -->

        <!-- Start latest-blog Area -->
        <section class="latest-blog-area section-gap" id="blog">
            <div class="<header id="header"></header>container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-60 col-lg-8">
                        <div class="title text-center">
                            <h1 class="mb-10">Latest News from our Blog</h1>
                            <p>Who are in extremely love with eco friendly system.</p>
                        </div>
                    </div>
                </div>					
                <div class="row">
                    <div class="col-lg-6 single-blog">
                        <img class="img-fluid" src="img/b1.jpg" alt="">
                        <ul class="tags">
                            <li><a href="#">Travel</a></li>
                            <li><a href="#">Life style</a></li>
                        </ul>
                        <a href="#"><h4>Portable latest Fashion for young women</h4></a>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore.
                        </p>
                        <p class="post-date">31st January, 2018</p>
                    </div>
                    <div class="col-lg-6 single-blog">
                        <img class="img-fluid" src="img/b2.jpg" alt="">
                        <ul class="tags">
                            <li><a href="#">Travel</a></li>
                            <li><a href="#">Life style</a></li>
                        </ul>
                        <a href="#"><h4>Portable latest Fashion for young women</h4></a>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore.
                        </p>
                        <p class="post-date">31st January, 2018</p>
                    </div>						
                </div>
            </div>	
        </section>
        <!-- End latest-blog Area -->


        <!-- start footer Area -->		
        <footer id="footer"></footer>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>			
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
        <script src="js/easing.min.js"></script>			
        <script src="js/hoverIntent.js"></script>
        <script src="js/superfish.min.js"></script>	
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>	
        <script src="js/owl.carousel.min.js"></script>			
        <script src="js/jquery.sticky.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>			
        <script src="js/parallax.min.js"></script>		
        <script src="js/mail-script.js"></script>	
        <script src="js/main.js"></script>	
    </body>
</html>
