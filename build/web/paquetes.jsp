<%-- 
    Document   : paquetes
    Created on : Jul 13, 2019, 8:05:36 AM
    Author     : nochtli13
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="codepixer">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>Startup</title>

        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
        <!--
        CSS
        ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/nice-select.css">					
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="js/utilities/imports.js"></script>
    </head>
    <body>
        <header id="header"></header>
        <!-- Start price Area -->
        <section class="price-area section-gap" id="price">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-60 col-lg-8">
                        <div class="title text-center">
                            <h1 class="mb-10">Choose the Perfect Plan for you</h1>
                            <p>Who are in extremely love with eco friendly system.</p>
                        </div>
                    </div>
                </div>					
                <div class="row">
                    <div class="col-lg-4">
                        <div class="single-price">
                            <div class="top-sec d-flex justify-content-between">
                                <div class="top-left">
                                    <h4>Standard</h4>
                                    <p>For the <br>individuals</p>
                                </div>
                                <div class="top-right">
                                    <h1>£199</h1>
                                </div>
                            </div>
                            <div class="bottom-sec">
                                <p>
                                    “Few would argue that, despite the advancements
                                </p>
                            </div>
                            <div class="end-sec">
                                <ul>
                                    <li>2.5 GB Free Photos</li>
                                    <li>Secure Online Transfer Indeed</li>
                                    <li>Unlimited Styles for interface</li>
                                    <li>Reliable Customer Service</li>
                                    <li>Manual Backup Provided</li>
                                </ul>
                                <button class="primary-btn price-btn mt-20">Purchase Plan</button>
                            </div>								
                        </div> 
                    </div>
                    <div class="col-lg-4">
                        <div class="single-price">
                            <div class="top-sec d-flex justify-content-between">
                                <div class="top-left">
                                    <h4>Business</h4>
                                    <p>For the <br>small Company</p>
                                </div>
                                <div class="top-right">
                                    <h1>£399</h1>
                                </div>
                            </div>
                            <div class="bottom-sec">
                                <p>
                                    “Few would argue that, despite the advancements
                                </p>
                            </div>
                            <div class="end-sec">
                                <ul>
                                    <li>2.5 GB Free Photos</li>
                                    <li>Secure Online Transfer Indeed</li>
                                    <li>Unlimited Styles for interface</li>
                                    <li>Reliable Customer Service</li>
                                    <li>Manual Backup Provided</li>
                                </ul>
                                <button class="primary-btn price-btn mt-20">Purchase Plan</button>
                            </div>								
                        </div> 
                    </div>	
                    <div class="col-lg-4">
                        <div class="single-price">
                            <div class="top-sec d-flex justify-content-between">
                                <div class="top-left">
                                    <h4>Ultimate</h4>
                                    <p>For the <br>large Company</p>
                                </div>
                                <div class="top-right">
                                    <h1>£499</h1>
                                </div>
                            </div>
                            <div class="bottom-sec">
                                <p>
                                    “Few would argue that, despite the advancements
                                </p>
                            </div>
                            <div class="end-sec">
                                <ul>
                                    <li>2.5 GB Free Photos</li>
                                    <li>Secure Online Transfer Indeed</li>
                                    <li>Unlimited Styles for interface</li>
                                    <li>Reliable Customer Service</li>
                                    <li>Manual Backup Provided</li>
                                </ul>
                                <button class="primary-btn price-btn mt-20">Purchase Plan</button>
                            </div>								
                        </div> 
                    </div>							

                </div>
            </div>	
        </section>
        <!-- End price Area -->
        <!-- start footer Area -->		
        <footer id="footer"></footer>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>			
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
        <script src="js/easing.min.js"></script>			
        <script src="js/hoverIntent.js"></script>
        <script src="js/superfish.min.js"></script>	
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>	
        <script src="js/owl.carousel.min.js"></script>			
        <script src="js/jquery.sticky.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>			
        <script src="js/parallax.min.js"></script>		
        <script src="js/mail-script.js"></script>	
        <script src="js/main.js"></script>	
    </body>
</html>
